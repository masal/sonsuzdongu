function Site() {

}

Site.URL_PAGES = BASE_URL + 'index/total-pages';
Site.URL_SHOW_PRODUCTS = BASE_URL + 'index/show';

Site.ID_PAGES = '#pages';
Site.ID_PRODUCTS = '#products';
Site.ID_ALERT = '#alert';

Site.prototype.bind = function () {
    var that = this;

    $('#fb-login').click(function (event) {
        that.onFacebookButtonClick(event);
    });

    this.loadPages();
    this.showProducts(0);
};

Site.prototype.onFacebookButtonClick = function (event) {
    event.preventDefault();

    Facebook.Login(function (response) {
        console.info(response);
        //do something with response...
    });
};

Site.prototype.loadPages = function () {
    var that = this;

    $.getJSON(Site.URL_PAGES, {}, function (jsonData) {
        var i, page;

        for (i = 1; i <= 10; i += 1) {
            page = '<li class="page"><a href="#">' + i + '</a></li>';
            $(Site.ID_PAGES).find('ul').append(page);
        }

        $('.page').click(function (event) {
            var value = $(this).text();

            event.preventDefault();

            $('.active').removeClass('active');
            $(this).addClass('active');

            that.showProducts(value);
        });
    });
};

Site.prototype.showImage = function (event) {
    if (!Facebook.Logged) {
        event.preventDefault();
        $(Site.ID_ALERT).modal();
    }
};

Site.prototype.showProducts = function (page) {
    var parameters = {'page': page },
        that = this;

    $.post(Site.URL_SHOW_PRODUCTS, parameters, function (data) {
        $(Site.ID_PRODUCTS).html(data);

        Currency.Convert('.money');
        GenericUIComponent.BindFade();

        $('.product-box').animate({opacity: 0.6});

        $('.original-image').click(function (event) {
            that.showImage(event);
        });

    });
};


function Twitter() {

}

Twitter.URL_SHOW = BASE_URL + 'twitter/show';
Twitter.ID_TWITTER = '#twitter';

Twitter.prototype.load = function () {
    var that = this;

    $.getJSON(Twitter.URL_SHOW, {}, function (data) {
        if (data.status === 'done') {
            console.info(data);

            that.showTweets(data.tweets);
        }
    });
};

Twitter.prototype.showTweets = function (tweets) {

    $(Twitter.ID_TWITTER).html(''); //Clear..

    $.each(tweets, function (index, tweet) {
        var message = '<div class="well-small tweet">' + tweet + '</div>';
        $(Twitter.ID_TWITTER).append(message);
    });
};

function Currency() {

}

Currency.Convert = function (id) {
    $.currency.configure({
        baseCurrency: "TRY",
        rates: {
            'USD': 0.55
        }
    });
    $(id).currency("USD");
};

// Facebook component wrapper
function Facebook() {

}

Facebook.Logged = false;

Facebook.prototype.prepare = function () {
    // Additional JS functions here
    window.fbAsyncInit = function () {
        FB.init({
            appId: '139671076166163', // App ID
            channelUrl: '//channel.html', // Channel File
            status: true, // check login status
            cookie: true, // enable cookies to allow the server to access the session
            xfbml: true  // parse XFBML
        });
        // Additional init code here
        FB.getLoginStatus(function (response) {
            if (response.status === 'connected') {
                $('#fb-login').hide();
                Facebook.Logged = true;
            } else {
                Facebook.Logged = false;
            }
        });
    };

    // Load the SDK Asynchronously
    (function (d) {
        var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement('script');
        js.id = id;
        js.async = true;
        js.src = "//connect.facebook.net/en_US/all.js";
        ref.parentNode.insertBefore(js, ref);
    }(document));
};

Facebook.Login = function (onSuccess) {
    onSuccess = onSuccess || function () {
    };

    FB.login(function (response) {
        var site = new Site();

        if (response.authResponse) {
            // connected
            console.info(response);
            onSuccess(response);
            Facebook.Logged = true;

            site.showProducts(0); // Refresh page..
        } else {
            Facebook.Logged = false;
            site.showProducts(0); // Refresh page..
        }
    });
};


function GenericUIComponent() {

}

GenericUIComponent.BindFade = function () {
    $('.product-box').mouseenter(function () {
        $(this).animate({'opacity': 1.0});
    });

    $('.product-box').mouseleave(function () {
        $(this).animate({'opacity': 0.5});
    });
};

$(document).ready(function () {
    var facebook = new Facebook(),
        site = new Site(),
        twitter = new Twitter();

    facebook.prepare();
    twitter.load();

    site.bind();

});