<?php
/**
 *
 * User: hasanozdemir
 * Date: 10.03.13
 * Time: 21:59
 */
class Entity
{

    private $id;

    private $entity;


    function __construct($id, $entity)
    {
        $this->id = $id;
        $this->entity = $entity;
    }

    public function setEntity($entity)
    {
        $this->entity = $entity;
    }

    public function getEntity()
    {
        return $this->entity;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }


}
