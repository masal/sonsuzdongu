<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Product
 *
 * @Table(name="product")
 * @Entity
 */
class Product
{

    static function create($pid, $name, $url, $price, $discountedPrice, $picturePath, $description){
        $product = new Product();

        $product->setPid($pid);
        $product->setDiscountedPrice($discountedPrice);
        $product->setName($name);
        $product->setPicturePath($picturePath);
        $product->setPrice($price);
        $product->setUrl($url);
        $product->setDescription($description);

        return $product;
    }
    /**
     * @var integer $id
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $name
     *
     * @Column(name="name", type="string", length=128, nullable=false)
     */
    private $name;

    /**
     * @var string $url
     *
     * @Column(name="url", type="string", length=255, nullable=false)
     */
    private $url;

    /**
     * @var float $price
     *
     * @Column(name="price", type="float", nullable=false)
     */
    private $price;

    /**
     * @var float $discountedPrice
     *
     * @Column(name="discounted_price", type="float", nullable=true)
     */
    private $discountedPrice;

    /**
     * @var string $picturePath
     *
     * @Column(name="picture_path", type="string", length=255, nullable=false)
     */
    private $picturePath;

    /**
     * @var integer $pid
     *
     * @Column(name="pid", type="integer", nullable=false)
     */
    private $pid;

    /**
     * @var text $description
     *
     * @Column(name="description", type="text", nullable=false)
     */
    private $description;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set url
     *
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set price
     *
     * @param float $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * Get price
     *
     * @return float 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set discountedPrice
     *
     * @param float $discountedPrice
     */
    public function setDiscountedPrice($discountedPrice)
    {
        $this->discountedPrice = $discountedPrice;
    }

    /**
     * Get discountedPrice
     *
     * @return float 
     */
    public function getDiscountedPrice()
    {
        return $this->discountedPrice;
    }

    /**
     * Set picturePath
     *
     * @param string $picturePath
     */
    public function setPicturePath($picturePath)
    {
        $this->picturePath = $picturePath;
    }

    /**
     * Get picturePath
     *
     * @return string 
     */
    public function getPicturePath()
    {
        return $this->picturePath;
    }

    /**
     * Set pid
     *
     * @param integer $pid
     */
    public function setPid($pid)
    {
        $this->pid = $pid;
    }

    /**
     * Get pid
     *
     * @return integer 
     */
    public function getPid()
    {
        return $this->pid;
    }

    /**
     * Set description
     *
     * @param text $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Get description
     *
     * @return text 
     */
    public function getDescription()
    {
        return $this->description;
    }

    public function copy(Product $other){
        $this->name = $other->name;
        $this->picturePath = $other->picturePath;
        $this->discountedPrice = $other->discountedPrice;
        $this->price = $other->price;
        $this->url = $other->url;
        $this->description = $other->description;

        return $this;
    }
}