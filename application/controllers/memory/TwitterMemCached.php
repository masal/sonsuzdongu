<?php
/**
 *
 * User: hasanozdemir
 * Date: 10.03.13
 * Time: 21:54
 */
include_once APPLICATION_PATH . '/controllers/Includes.php';

class TwitterMemCached
{
    const MAX_EXPIRATION_TIME = 300;

    const TWITTER = 'twitter';

    /**
     * @var Memcached
     */
    private $memory;

    function __construct()
    {
        $this->memory = new Memcached();
        $this->memory->addServer("localhost", 11211);
        $this->memory->setOption(Memcached::OPT_VERIFY_KEY || Memcached::OPT_HASH ||Memcached::OPT_DISTRIBUTION , true);
    }

    function add(Entity $entity){
        $twitter = $this->memory->get(self::TWITTER);
        $twitter[$entity->getId()] = $entity->getEntity();
        $this->memory->replace(self::TWITTER, $twitter);
        return true;
    }

    public function findAll(){
        $cached = $this->memory->get(self::TWITTER);

        if(!$cached){
            return array();
        }

        $entities = array();

        foreach($cached as $id => $entity){
            $entities[] = $entity;
        }
        return $entities;
    }

    function get($key){
        return $this->memory->get($key);
    }
}
