<?php
/**
 *
 * User: hasanozdemir
 * Date: 10.03.13
 * Time: 22:02
 */

include_once APPLICATION_PATH . '/../console/controller/xml/XMLParser.php';
include_once APPLICATION_PATH . '/../console/controller/xml/AbstractXMLParser.php';
include_once APPLICATION_PATH . '/controllers/memory/TwitterMemCached.php';

class TwitterXMLParser extends AbstractXMLParser
{

    private $twitterMem;

    function __construct(TwitterMemCached $twitterMem)
    {
        $this->twitterMem = $twitterMem;
    }

    protected function handleSimpleXMLElement(SimpleXMLElement $element)
    {
        $entity = $this->twitterMem->get($element->id + '');

        if($entity == null){
            $entity = new Entity($element->id . '', $element->text . '');
            $this->twitterMem->add($entity);
        }

        $this->add($entity);
    }
}
