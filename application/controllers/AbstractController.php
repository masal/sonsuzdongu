<?php

use Doctrine\ORM\EntityManager;

/**
 * User: hasanozdemir
 */
abstract class AbstractController extends Zend_Controller_Action
{
    /**
     * @var Doctrine\ORM\EntityManager
     */
    protected $entityManager = null;

    public function init()
    {
        $registry = Zend_Registry::getInstance();
        $this->entityManager = $registry->entitymanager;
    }

}
