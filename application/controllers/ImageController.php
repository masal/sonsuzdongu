<?php

include_once APPLICATION_PATH . '/controllers/Includes.php';

/**
 *
 * User: hasanozdemir
 * Date: 10.03.13
 * Time: 19:33
 */
class ImageController extends AbstractController
{

    public function showAction()
    {
        $request = $this->getRequest();
        $productId = $request->getParam('product-id');

        if (empty($productId)) {
            $this->_helper->_json(array('error' => 'Product id empty!'));
        }

        $product = $this->entityManager->find('Product', $productId);

        if ($product == null) {
            $this->_helper->_json(array("error" => "Product does not exist!"));
        }

        $this->_helper->viewRenderer->setNoRender();
        $raw = imagecreatefromjpeg($product->getPicturePath());

        header('Content-type: image/jpeg');
        imagejpeg($raw, null, 100);
        exit();
    }

}
