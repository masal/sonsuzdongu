<?php
/**
 *
 * User: hasanozdemir
 * Date: 10.03.13
 * Time: 21:52
 */
define('REAL_PATH', APPLICATION_PATH . '/../console/');

include_once APPLICATION_PATH . '/controllers/Includes.php';

include_once APPLICATION_PATH . '/../console/controller/http/ZendHttpClient.php';
include_once APPLICATION_PATH . '/controllers/memory/TwitterMemCached.php';
include_once APPLICATION_PATH . '/controllers/xml/TwitterXMLParser.php';


class TwitterController extends AbstractController
{
    const TWITTER_URL = 'https://api.twitter.com/1/statuses/user_timeline.xml?screen_name=yuxel';

    /**
     * @var ZendHttpClient
     */
    private $zendHttpClient;

    /**
     * @var TwitterMemCached
     */
    private $twitterMemcached;

    /**
     * @var TwitterXMLParser
     */
    private $twitterParser;

    public function init()
    {
        parent::init();
        $this->twitterMemcached = new TwitterMemCached();
        $this->zendHttpClient = new ZendHttpClient();

        $this->twitterParser = new TwitterXMLParser($this->twitterMemcached);
    }

    public function indexAction()
    {

    }


    public function showAction()
    {
        $entities = $this->twitterMemcached->findAll();

        if(sizeof($entities) > 0){
            $this->_helper->_json(array('status' => 'done', 'tweets' => $entities));
            return;
        }

        // Otherwise reload twitter data and cache it.

        $rawData = $this->zendHttpClient->retrieve(TwitterController::TWITTER_URL);
        $this->twitterParser->parse($rawData);

        $cached = $this->twitterMemcached->findAll();
        $this->_helper->_json(array('status' => 'done', 'tweets' => $cached));
    }
}
