<?php

include_once APPLICATION_PATH . '/controllers/Includes.php';

class IndexController extends AbstractController
{

    const PRODUCT_LIMIT = 8;

    /**
     * @var ProductDao
     */
    private $productDao;

    public function init()
    {
        parent::init();
        $this->productDao = new DoctrineProductDao($this->entityManager);
    }

    public function indexAction()
    {

    }

    public function showAction()
    {
        $this->_helper->layout()->disableLayout();

        $request = $this->getRequest();
        $page = $request->getParam('page', 1);

        $offset = $page * IndexController::PRODUCT_LIMIT;

        $products = $this->productDao->findWith($offset, IndexController::PRODUCT_LIMIT);

        $this->view->assign('products', $products);
    }

    public function totalPagesAction()
    {
        $totalProducts = $this->productDao->countAll();
        $totalPages = ceil($totalProducts / IndexController::PRODUCT_LIMIT);

        $this->_helper->_json(array('pages' => $totalPages));
    }
}