<?php
/**
 *
 * User: hasanozdemir
 * Date: 10.03.13
 * Time: 16:43
 */
interface ProductDao
{
    /**
     * merges array of products.
     *
     * @param array $products
     * @return mixed
     */
    public function merge(array $products);

    public function findWith($offset, $limit);

    public function countAll();
}
