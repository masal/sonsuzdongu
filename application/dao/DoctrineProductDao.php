<?php
/**
 *
 * User: hasanozdemir
 * Date: 10.03.13
 * Time: 16:44
 */
class DoctrineProductDao implements ProductDao
{

    const BATCH_SIZE = 20;

    /**
     * @var Doctrine\ORM\EntityManager
     */
    private $entityManager;

    function __construct(\Doctrine\ORM\EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }


    /**
     * saves array of products.
     *
     * @param array $products
     * @return mixed
     */
    public function merge(array $products)
    {
        $i = 0;

        foreach ($products as $product) {
            $existing = $this->entityManager->getRepository('Product')->findOneBy(array('pid' => $product->getPid()));
            if ($existing != null) {
                $existing = $existing->copy($product);

                $this->entityManager->merge($existing);
            } else {

                $this->entityManager->persist($product);
            }

            $i++;
            if ($i % ProductController::BATCH_SIZE == 0) {
                $this->entityManager->flush();
                $this->entityManager->clear();
            }
        }
    }

    public function findWith($offset, $limit)
    {
        return $this->entityManager->getRepository('Product')->findBy(array(), null, $limit, $offset);
    }

    public function countAll(){
        return sizeof($this->entityManager->getRepository('Product')->findAll() );
    }


}
