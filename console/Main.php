<?php

define('APPLICATION_ENV', 'development');
define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));
define('REAL_PATH', realpath(dirname(__FILE__)));

// Change this path, if you want to store image in a different location.
define('IMAGES_DIRECTORY', REAL_PATH . '/../images/');

// Default image width and height, used to resize images.
define('IMAGE_WIDTH', 165);
define('IMAGE_HEIGHT', 170);

include_once REAL_PATH . '/util/Logger.php';

set_include_path(implode(PATH_SEPARATOR, array(realpath(APPLICATION_PATH . '/../library'), get_include_path())));

include_once REAL_PATH . '/controller/ProductController.php';

// Zend Components
require_once 'Zend/Application.php';

// Create application
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);

$application->getBootstrap()->bootstrap('doctrine');
$em = $application->getBootstrap()->getResource('doctrine');

if($em == null){
    throw new RuntimeException('Entity manager is null. Check your configuration.');
}

$productController = new ProductController($em);
$productController->loadProducts(ProductController::TYPE_BUTIGO);

?>

