<?php
/**
 *
 * User: hasanozdemir
 * Date: 10.03.13
 * Time: 14:32
 */
include_once REAL_PATH . '/controller/xml/XMLParser.php';

abstract class AbstractXMLParser implements XMLParser
{

    private $eventListeners;

    private $items;

    function __construct()
    {
        $this->eventListeners = array();
        $this->items = array();
    }

    public function parse($xmlData)
    {
        $content = simplexml_load_string($xmlData);

        if (!$content) {
            throw new Exception('File could not be read!');
        }

        foreach ($content as $simpleElement) {
            $this->handleSimpleXMLElement($simpleElement);
        }

        return $this->items;
    }

    public function register(EventListener $eventListener)
    {
        $this->eventListeners[] = $eventListener;
    }


    protected final function add($item)
    {
        $this->items[] = $item;
    }

    protected function fireEventListener($type, $event)
    {
        foreach ($this->eventListeners as $eventListener) {
            if ($eventListener->supports($type)) {
                $eventListener->newEvent($event);
            }
        }
    }

    protected abstract function handleSimpleXMLElement(SimpleXMLElement $element);

}
