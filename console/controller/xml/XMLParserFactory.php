<?php
/**
 *
 * User: hasanozdemir
 * Date: 10.03.13
 * Time: 14:06
 */
class XMLParserFactory
{
    const BUTIGO = "ButigoXMLParser";

    /**
     * Creates a new instance of requested type.
     * @param $type
     *      a type of xml parser.
     * @return XMLParser in passed type.
     * @throws Exception
     *          if type could not be found.
     */
    public static function create($type)
    {

        if (include_once REAL_PATH . '/controller/xml/' . $type . '.php') {
            return new $type;
        } else {
            throw new Exception('Type [' . $type . ' not found!]');
        }
    }
}
