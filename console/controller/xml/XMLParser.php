<?php
/**
 * XMLParser describes operations to parse an XML file. Each xml processor is also responsible to give an information
 * which types.
 *
 * User: hasanozdemir
 * Date: 10.03.13
 * Time: 01:55
 */
interface XMLParser
{

    /**
     * parses passed XML Data to retrieve product instances.
     *
     * @param $xmlData
     *      string xml data
     * @return array of products
     */
    public function parse($xmlData);

    public function register(EventListener $eventListener);
}
