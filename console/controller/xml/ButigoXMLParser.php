<?php
/**
 * User: hasanozdemir
 * Date: 10.03.13
 * Time: 02:21
 */
include_once REAL_PATH . '/controller/xml/AbstractXMLParser.php';

include_once APPLICATION_PATH . '/models/Product.php';

class ButigoXMLParser extends AbstractXMLParser
{
    protected function handleSimpleXMLElement(SimpleXMLElement $element)
    {
        $path = IMAGES_DIRECTORY . $element->ID . '.jpg';

        $product = Product::create( $element->ID, $element->NAME,
                                    $element->IMAGE, $element->ORIGINAL_PRICE,
                                    $element->DISCOUNTED_PRICE, $path, $element->DESCRIPTION);

        $this->fireEventListener(EventListener::IMAGE_EVENT, $product);

        $this->add($product);
    }
}
