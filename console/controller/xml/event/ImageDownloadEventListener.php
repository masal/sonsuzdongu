<?php
/**
 *
 * User: hasanozdemir
 * Date: 10.03.13
 * Time: 14:55
 */

include_once REAL_PATH . '/controller/xml/event/EventListener.php';
include_once APPLICATION_PATH . '/models/Product.php';

/**
 * Is a reference implementation of EventListener.
 * This Event must be activated if a new product (includes an image) received.
 */
class ImageDownloadEventListener implements EventListener
{

    public function newEvent(Product $eventItem)
    {
        $imageContent = file_get_contents($eventItem->getUrl());

        if (!$imageContent) {
            throw new Exception('File could not be downloaded!');
        }

        $result = file_put_contents($eventItem->getPicturePath(), $imageContent);

        if (!$result) {
            throw new Exception('File could not be saved!');
        }

        Logger::info('File successfully saved to ' . $result);
    }

    public function supports()
    {
        return EventListener::IMAGE_EVENT;
    }
}
