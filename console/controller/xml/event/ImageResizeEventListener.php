<?php
/**
 * Created by JetBrains PhpStorm.
 * User: hasanozdemir
 * Date: 10.03.13
 * Time: 02:19
 * To change this template use File | Settings | File Templates.
 */
include_once REAL_PATH . '/controller/xml/event/EventListener.php';

class ImageResizeEventListener implements EventListener
{
    const QUALITY = 100;

    public function newEvent(Product $eventItem)
    {
        Logger::info("Resizing image in path ' " . $eventItem->getPicturePath() . "'");

        $source = $eventItem->getPicturePath();
        list($width, $height) = getimagesize($source) ;

        $target = imagecreatetruecolor(IMAGE_WIDTH, IMAGE_HEIGHT) ;

        $image = imagecreatefromjpeg($source) ;

        imagecopyresampled($target, $image, 0, 0, 0, 0, IMAGE_WIDTH, IMAGE_HEIGHT, $width, $height) ;

        imagejpeg($target, $source, ImageResizeEventListener::QUALITY);

        Logger::info('Image ( ' . $eventItem->getName() . ' ) has been resized successfully');
    }

    public function supports()
    {
        return EventListener::IMAGE_EVENT;
    }
}
