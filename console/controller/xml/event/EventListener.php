<?php
/**
 *
 * User: hasanozdemir
 * Date: 10.03.13
 * Time: 02:19
 * To change this template use File | Settings | File Templates.
 */
include_once APPLICATION_PATH . '/models/Product.php';

interface EventListener
{
    const IMAGE_EVENT = "image";

    /**
     * must be called, if new supported event is fired.
     *
     * @param Product $eventItem
     *
     * @return void
     */
    public function newEvent(Product $eventItem);

    /**
     * @return mixed, which type of events is supported.
     */
    public function supports();
}
