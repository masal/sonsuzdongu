<?php

include_once REAL_PATH . '/controller/http/HttpClient.php';
/**
 * User: hasanozdemir
 * Date: 10.03.13
 * Time: 02:01
 * To change this template use File | Settings | File Templates.
 */
class ZendHttpClient implements HttpClient
{

    private static $CONTENT_LENGTH = "Content-Length";

    /**
     * retrieves a file from passed file URL.
     *
     * @param $fileUrl
     *          a valid HTTP url to download file.
     * @return mixed
     *          content of retrieved file
     *
     * @throws Exception
     *      if file does not exist.
     */
    public function retrieve($fileUrl)
    {
        $client = new Zend_Http_Client($fileUrl);

        $response = $client->request();

        if($response->getHeader(ZendHttpClient::$CONTENT_LENGTH) == NULL){
            throw new Exception('File does not exist!');
        }

        Logger::info('File downloaded successfully! Great job!');

        return $response->getBody();
    }
}
