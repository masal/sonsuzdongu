<?php
/**
 * User: hasanozdemir
 * Date: 10.03.13
 * Time: 01:59
 * To change this template use File | Settings | File Templates.
 */
interface HttpClient
{
    /**
     * retrieves a file from passed file URL.
     *
     * @param $fileUrl
     *          a valid HTTP url to download file.
     * @return mixed
     *       content of retrieved file.
     */
    public function retrieve($fileUrl);
}
