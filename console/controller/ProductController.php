<?php
/**
 *
 * User: hasanozdemir
 * Date: 10.03.13
 * Time: 14:13
 */

include_once REAL_PATH . '/controller/http/ZendHttpClient.php';
include_once REAL_PATH . '/controller/xml/XMLParserFactory.php';

include_once REAL_PATH . '/controller/xml/event/ImageDownloadEventListener.php';
include_once REAL_PATH . '/controller/xml/event/ImageResizeEventListener.php';

include_once APPLICATION_PATH . '/dao/ProductDAO.php';
include_once APPLICATION_PATH . '/dao/DoctrineProductDAO.php';

class ProductController
{

    const BUTIGO_URL = "http://www.butigo.com/download/product-feed/current.xml";

    const TYPE_BUTIGO = 1;

    const BATCH_SIZE = 20;

    /**
     * @var ZendHttpClient
     */
    private $zendHttpClient;

    /**
     * @var ProductDao
     */
    private $productDao;

    /**
     * @var Doctrine\ORM\EntityManager
     */
    private $entityManager;

    function __construct(\Doctrine\ORM\EntityManager $entityManager)
    {
        $this->zendHttpClient = new ZendHttpClient();
        $this->productDao = new DoctrineProductDao($entityManager);
        $this->entityManager = $entityManager;
    }

    public function loadProducts($type)
    {
        switch ($type) {
            case ProductController::TYPE_BUTIGO:
                $this->loadButigoProducts();
                break;
            default:
                throw new Exception('Unsupported type [' . $type . ']');
        }
    }

    private function loadButigoProducts()
    {
        $data = $this->zendHttpClient->retrieve(ProductController::BUTIGO_URL);

        $xmlParser = XMLParserFactory::create(XMLParserFactory::BUTIGO);

        $xmlParser->register(new ImageDownloadEventListener());
        $xmlParser->register(new ImageResizeEventListener());

        $products = $xmlParser->parse($data);

        try {
            $this->entityManager->getConnection()->beginTransaction();

            $this->productDao->merge($products);

            $this->entityManager->getConnection()->commit();

            Logger::info("Processing finished successfully.");
        } catch (Exception $e) {
            Logger::error('Products could not be saved! Rolling back changed.');
            $this->entityManager->getConnection()->rollback();
        }


    }

}
