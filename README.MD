## Sonsuz Dongu Sample

### Database configuration
    Change database settings configured in application/configs/application.ini

### Memcached
    Memcached must be installed and configured on your environment! Used default host and ports on this sample application.
    ( "localhost", 11211)

### Running console application

#### Pre-Condition

    After configuring your database, you must create a table 'product'. Use scripts/db-create.sql to create this table.
#### Executing
     'php Main.php' in console folder


I recommend to configure a virtual host to run this application, sample (don't forget to change hosts configuration):
####
    <VirtualHost *:80>
        ServerName sonsuzdongu.localhost
        DocumentRoot ABSOLUTE_PATH/sonsuzdongu/public
        SetEnv APPLICATION_ENV "development"
        <Directory ABSOLUTE_PATH/sonsuzdongu/public>
            Options +Indexes +FollowSymLinks +ExecCGI
            DirectoryIndex index.php
            AllowOverride All
            Order allow,deny
            Allow from all
        </Directory>
    </VirtualHost>

hosts :  127.0.0.1 sonsuzdongu.localhost localhost



